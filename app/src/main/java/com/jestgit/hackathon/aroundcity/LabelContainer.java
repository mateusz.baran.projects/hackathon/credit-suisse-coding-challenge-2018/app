package com.jestgit.hackathon.aroundcity;

import android.content.Context;

import org.json.*;

import java.util.ArrayList;

public class LabelContainer {
    private ArrayList<Label> labelArrayList;
    private Context context;

    public LabelContainer(Context context) {
        this.context = context;
        labelArrayList = new ArrayList<>();
    }

    public void initList() {
        labelArrayList.clear();
        labelArrayList.add(new Label(new Place("Biedronka Grunwald", " ", 51.110910, 17.057002), context));
        labelArrayList.add(new Label(new Place("Biedronka Rynek", " ", 51.109225, 17.034664), context));
        labelArrayList.add(new Label(new Place("Biedronka Premium", " ", 51.100357, 17.039952), context));
        labelArrayList.add(new Label(new Place("Biedronka Radek", " ", 51.116033, 17.052813), context));
        labelArrayList.add(new Label(new Place("Biedronka Biskupin", " ", 51.105341, 17.081317), context));
        labelArrayList.add(new Label(new Place("Zabka Grunwaldzki Center", " ", 51.110891, 17.062423), context));
        labelArrayList.add(new Label(new Place("Zabka Grunwald", " ", 51.111788, 17.058200), context));
        labelArrayList.add(new Label(new Place("Zabka Poczta", " ", 51.114503, 17.060435), context));
        labelArrayList.add(new Label(new Place("Zabka Botaniczny", " ", 51.116403, 17.052581), context));

    }

    public void loadFromJSON(String jsonString, int index) throws JSONException {
        labelArrayList.clear();

        JSONArray arr = new JSONArray(jsonString);
        arr = arr.getJSONArray(index);

        String name, description;
        double latitude, longitude;

        for (int i = 0; i < arr.length(); i++) {
            name = arr.getJSONObject(i).getString("name");
            description = arr.getJSONObject(i).getString("description");
            latitude = arr.getJSONObject(i).getDouble("latitude");
            longitude = arr.getJSONObject(i).getDouble("longitude");
            labelArrayList.add(new Label(new Place(name, description, longitude, latitude), context));
        }
    }

    public ArrayList<Label> getLabelArrayList() {
        return labelArrayList;
    }
}
