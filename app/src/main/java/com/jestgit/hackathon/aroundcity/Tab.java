package com.jestgit.hackathon.aroundcity;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

public class Tab {
    private static final int WIDTH = 160;
    private static final int PADDING = 20;

    private String name;
    private TextView textView;

    public Tab(final Context c, String name, int screenWidth, int y, int colorIdm, int iconId) {
        this.name = name;

        textView = new TextView(c);
        textView.setX(screenWidth - WIDTH - PADDING);
        textView.setY(y);
        textView.setMaxLines(1);
        textView.setTypeface(Typeface.DEFAULT_BOLD);
        textView.setBackground(c.getDrawable(R.drawable.custom_tab_view));
        textView.setText(name);
        textView.setTextSize(20);
        textView.setVisibility(View.VISIBLE);
        textView.setTextColor(Color.BLACK);
        //textView.getLayoutParams().width = ViewGroup.LayoutParams.WRAP_CONTENT;
        textView.setWidth(WIDTH);
        textView.setGravity(Gravity.END);
        textView.setId(Character.getNumericValue(name.charAt(0)));
        textView.setCompoundDrawablesWithIntrinsicBounds(iconId, 0, 0, 0);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TextView getTextView() {
        return textView;
    }
}
