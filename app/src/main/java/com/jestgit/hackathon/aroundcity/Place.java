package com.jestgit.hackathon.aroundcity;

public class Place {
    private long id;
    private String name;
    private String description;
    private double longitude;
    private double latitude;

    public Place(String name, String descritpion, double longitude, double latitude) {
        this.name = name;
        this.description = descritpion;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }
}
