package com.jestgit.hackathon.aroundcity;


import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.Gravity;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.TextView;

public class Label {
    private Place place;
    private double x;
    private double y;
    private TextView textView;

    public Label(Place place,  Context c) {
        this.x = 0;
        this.y = 0;
        this.place = place;
        textView = new TextView(c);
        textView.setMaxLines(1);
        textView.setTypeface(Typeface.DEFAULT_BOLD);
        textView.setBackground(c.getDrawable(R.drawable.custom_text_view));
        textView.setText(getPlace().getName());
        textView.setVisibility(View.INVISIBLE);
        textView.setTextColor(Color.BLACK);

    }

    public Place getPlace() {
        return place;
    }

    public TextView getTextView() {
        return textView;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void setVisibility(boolean isVisible) {
        if (isVisible) {
            textView.setVisibility(View.VISIBLE);
        } else {
            textView.setVisibility(View.INVISIBLE);
        }
    }

}
