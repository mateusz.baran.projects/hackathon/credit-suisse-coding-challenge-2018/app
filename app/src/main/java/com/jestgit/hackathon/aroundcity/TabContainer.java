package com.jestgit.hackathon.aroundcity;

import android.content.Context;

import java.util.ArrayList;

public class TabContainer {
    private static final int TOP_PADDING = 270;
    private static final int LINESPACE = 200;

    private ArrayList<Tab> tabArrayList;
    private Context context;
    private int screenWidth;

    public TabContainer(Context context, int screenWidth) {
        this.context = context;
        this.screenWidth = screenWidth;
        initList();
    }

    public void initList() {
        tabArrayList = new ArrayList<>();
        tabArrayList.add(new Tab(context, "B", screenWidth, TOP_PADDING, 0, R.drawable.biedronka));
        tabArrayList.add(new Tab(context, "Z", screenWidth, TOP_PADDING + LINESPACE, 0, R.drawable.zabka));
        tabArrayList.add(new Tab(context, "S", screenWidth, TOP_PADDING + LINESPACE * 2, 0, R.drawable.galeria));

    }

    public ArrayList<Tab> getTabArrayList() {
        return tabArrayList;
    }
}
