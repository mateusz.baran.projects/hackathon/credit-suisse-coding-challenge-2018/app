package com.jestgit.hackathon.aroundcity;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class PlaceDescriptionFragment extends DialogFragment {
    private static final String ARG_NAME = "name";
    private static final String ARG_LONGITUDE = "longitude";
    private static final String ARG_LATITUDE = "latitude";
    private static final String ARG_DESCRIPTION = "description";

    private String name;
    private String description;
    private double longitude;
    private double latitude;

    public PlaceDescriptionFragment() {
        // Required empty public constructor
    }


    public static PlaceDescriptionFragment newInstance(Place place) {
        PlaceDescriptionFragment fragment = new PlaceDescriptionFragment();
        Bundle args = new Bundle();
        //Put data into bundle
        args.putString(ARG_NAME, place.getName());
        args.putString(ARG_DESCRIPTION, place.getDescription());
        args.putDouble(ARG_LATITUDE, place.getLatitude());
        args.putDouble(ARG_LONGITUDE, place.getLongitude());
        //Loads up bundle into Fragment
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            //Read data from bundle
            name = getArguments().getString(ARG_NAME, "Name");
            description = getArguments().getString(ARG_DESCRIPTION, "Desc");
            longitude = getArguments().getDouble(ARG_LONGITUDE, 0);
            latitude = getArguments().getDouble(ARG_LATITUDE, 0);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_place_description, container, false);
        initTextViews(view);

        return view;
    }

    private void initTextViews(View view) {
        //name
        TextView nameTextView = view.findViewById(R.id.text_view_name);
        nameTextView.setText(name);
        //description
        TextView descTextView = view.findViewById(R.id.text_view_desc);
        descTextView.setText(description);
        //longitude
        TextView longTextView = view.findViewById(R.id.text_view_long);
        longTextView.setText(String.valueOf(longitude));
        //lang
        TextView latTextView = view.findViewById(R.id.text_view_lat);
        latTextView.setText(String.valueOf(latitude));
    }

}
