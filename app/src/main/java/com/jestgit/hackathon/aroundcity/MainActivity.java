package com.jestgit.hackathon.aroundcity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.graphics.Point;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import android.hardware.Camera;
import android.support.annotation.NonNull;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.Toast;

import android.widget.SeekBar;

import org.json.JSONException;

public class MainActivity extends AppCompatActivity implements SensorEventListener, View.OnClickListener {
    private static final int REQUEST_CAMERA_PERMISSION = 200;

    private Camera mCamera;
    private CameraPreview mPreview;

    Compass compass;
    Localizer localizer;
    Interpreter interpreter;
    SensorManager mSensorManager;

    private RelativeLayout relativeLayout;
    private int screenWidth;
    private int screenHeight;
    private double h;
    private int textSize;
    private ArrayList<Label> labelsList;
    private LabelContainer labelContainer;
    private ArrayList<Tab> tabsList;
    private TabContainer tabContainer;
    private VerticalSeekBar seekBar;

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Hide status bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        initializeVariables();
        initLists();
        checkLocalizationPermission();
        checkCameraPermission();
        initializeCamera();

        initCompass();
        initLocalizer();
        initInterpreter();
        initAccelSensor();
        initSeekBarListener();

        addTextViews();
    }

    private void initLabelTouchListeners() {
        for (Label label : labelsList) {
            //final Place place = new Place(label.getName(), "Description", label.getLongitude(), label.getLatitude());
            final Place place = label.getPlace();
            label.getTextView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PlaceDescriptionFragment placeDescriptionFragment = PlaceDescriptionFragment.newInstance(place);
                    placeDescriptionFragment.show(getSupportFragmentManager(), null);
                }
            });
        }
    }

    private void initSeekBarListener() {
        seekBar = findViewById(R.id.verticalSeekbar);
        seekBar.setMax(5);
        seekBar.getProgressDrawable().setColorFilter(Color.WHITE, PorterDuff.Mode.MULTIPLY);
        seekBar.getThumb().setColorFilter(Color.DKGRAY, PorterDuff.Mode.SRC_ATOP);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                interpreter.setHorizon(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        double alpha = Math.round(compass.getRotationDegree() / 2.0) * 2.0;
        double x1 = Localizer.getLongitude();
        double y1 = Localizer.getLatitude();

        for (int i = 0; i < labelsList.size(); i++) {
            double x2 = labelsList.get(i).getPlace().getLongitude();
            double y2 = labelsList.get(i).getPlace().getLatitude();
            labelsList.get(i).setX(interpreter.getX(alpha, x1, y1, x2, y2));
            labelsList.get(i).setY(interpreter.getY(alpha, x1, y1, x2, y2));
            boolean isVisible = interpreter.isVisible(alpha, x1, y1, x2, y2);
            labelsList.get(i).setVisibility(isVisible);
        }

        updatePlaceLabels();
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
    }

    private void initAccelSensor() {
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
    }

    private void initLists() {
        labelContainer = new LabelContainer(getApplicationContext());
        labelsList = labelContainer.getLabelArrayList();

        tabContainer = new TabContainer(getApplicationContext(), screenWidth);
        tabsList = tabContainer.getTabArrayList();
    }

    private void sendRequestForJSON(int index) {
        // TODO zaimplementowac zapytanie do azura

        String json = "[[{\"id\":6,\"name\":\"Zabka Grunwaldzki Center\",\"description\":\"Curie-Skłodowskiej 45\",\"latitude\":51.110891,\"longitude\":17.062423,\"type\":\"Zabka\"},{\"id\":7,\"name\":\"Zabka Grunwald\",\"description\":\"Curie-Skłodowskiej 15\",\"latitude\":51.111788,\"longitude\":17.0582,\"type\":\"Zabka\"},{\"id\":8,\"name\":\"Zabka Poczta\",\"description\":\"Piastowska 20\",\"latitude\":51.114503,\"longitude\":17.060435,\"type\":\"Zabka\"},{\"id\":9,\"name\":\"Zabka Botaniczny\",\"description\":\"Sępa-Szarzyńskiego 48\",\"latitude\":51.116403,\"longitude\":17.052581,\"type\":\"Zabka\"}],[{\"id\":1,\"name\":\"Biedronka Grunwald\",\"description\":\"plac Grunwaldzki 12-14\",\"latitude\":51.11091,\"longitude\":17.057002,\"type\":\"Biedronka\"},{\"id\":2,\"name\":\"Biedronka Rynek\",\"description\":\"Szewska 6/7\",\"latitude\":51.109225,\"longitude\":17.034664,\"type\":\"Biedronka\"},{\"id\":3,\"name\":\"Biedronka Premium\",\"description\":\"plac Konstytucji 3 Maja 3\",\"latitude\":51.100357,\"longitude\":17.039952,\"type\":\"Biedronka\"},{\"id\":4,\"name\":\"Biedronka Radek\",\"description\":\"Benedyktyńska 15\",\"latitude\":51.116033,\"longitude\":17.052813,\"type\":\"Biedronka\"},{\"id\":5,\"name\":\"Biedronka Biskupin\",\"description\":\"Wróblewskiego 7a\",\"latitude\":51.105341,\"longitude\":17.081317,\"type\":\"Biedronka\"}]]";

        try {
            labelContainer.loadFromJSON(json, index);
            labelsList = labelContainer.getLabelArrayList();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        relativeLayout.removeAllViews();
        addTextViews();
        initLabelTouchListeners();

        updatePlaceLabels();
    }

    private void initCompass() {
        compass = new Compass(this);
        compass.start();
    }

    private void initLocalizer() {
        localizer = new Localizer(getApplicationContext(), 20, 1000);
        localizer.prepare();
        localizer.run();
    }

    private void initInterpreter() {
        interpreter = new Interpreter(mPreview.getHorizontalViewAngle(), 2, 2);
    }

    public void checkLocalizationPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            checkLocPermission();
        }
    }

    private void checkLocPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle("Permission")
                        .setMessage("turn on")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(MainActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }

    private void initializeVariables() {
        int lowerTabSize = 90;
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        screenWidth = size.x;
        screenHeight = (int) size.y - lowerTabSize;
        h = screenHeight / 2;
        textSize = 20;

        relativeLayout = (RelativeLayout) findViewById(R.id.relative);
    }

    private void placeLabel(Label label) {
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) label.getTextView().getLayoutParams();
        layoutParams.leftMargin = (int) (label.getX() * screenWidth - label.getTextView().getTextSize() * getApplicationContext().getResources().getDisplayMetrics().scaledDensity);
        layoutParams.topMargin = (int) (h - label.getY() * screenHeight / 2);
        label.getTextView().setLayoutParams(layoutParams);
        label.getTextView().setAlpha((float) (1 - (Math.abs(label.getY()))));
        label.getTextView().setTextSize((float) (textSize - textSize / 2 * (label.getY())));
    }

    private void updatePlaceLabels() {
        for (int i = 0; i < labelsList.size(); i++)
            placeLabel(labelsList.get(i));
    }

    private void addTextViews() {
        relativeLayout.removeAllViews();

        for (int i = 0; i < labelsList.size(); i++)
            relativeLayout.addView(labelsList.get(i).getTextView());

        for (int i = 0; i < tabsList.size(); i++) {
            relativeLayout.addView(tabsList.get(i).getTextView());
            tabsList.get(i).getTextView().setOnClickListener(this);
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case 11:
                sendRequestForJSON(1);
                break;
            case 35:
                sendRequestForJSON(0);
                break;
            case 28:
                //galeria function
                break;
        }
    }

    private void checkCameraPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CAMERA_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                // close the app
                Toast.makeText(MainActivity.this, "Ne permission granted", Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }

    private void initializeCamera() {
        mCamera = getCameraInstance();

        // Create our Preview view and set it as the content of our activity.
        mPreview = new CameraPreview(this, mCamera);
        FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
        preview.addView(mPreview);
    }

    /**
     * Get instance of Camera object
     */
    public static Camera getCameraInstance() {
        Camera c = null;
        try {
            c = Camera.open(); // attempt to get a Camera instance
        } catch (Exception e) {
            // Camera is not available (in use or does not exist)
        }
        return c; // returns null if camera is unavailable
    }

    @Override
    protected void onResume() {
        super.onResume();
        compass.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        compass.stop();
    }

    @Override
    protected void onStart() {
        super.onStart();
        compass.start();
    }

    @Override
    protected void onStop() {
        super.onStop();
        compass.stop();
    }
}
