package com.jestgit.hackathon.aroundcity;

public class Interpreter {
    private double angleOfView;
    private int horizon;
    private int horizonRange;

    public Interpreter(double angleOfView, int horizon, int horizonRange) {
        this.angleOfView = angleOfView;
        this.horizon = horizon;
        this.horizonRange = horizonRange;
    }

    public int getHorizon() {
        return horizon;
    }

    public void setHorizon(int horizon) {
        this.horizon = horizon;
    }

    public int getHorizonRange() {
        return horizonRange;
    }

    public void setHorizonRange(int horizonRange) {
        this.horizonRange = horizonRange;
    }

    private double getDistance(double x1, double y1, double x2, double y2) {
        return Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(Math.cos(x1 * Math.PI / 180) * (y2 - y1), 2)) * 40075.704 / 360.0;
    }

    public boolean isVisible(double alpha, double x, double y, double x_object, double y_object) {
        return Math.abs(gamma(alpha, x, y, x_object, y_object)) < 0.5 * this.angleOfView;
    }

    public double getX(double alpha, double x, double y, double x_object, double y_object) {
        return (.5 * this.angleOfView + gamma(alpha, x, y, x_object, y_object)) / this.angleOfView;
    }

    public double getY(double alpha, double x, double y, double x_object, double y_object) {
        return (getDistance(x_object, y_object, x, y) - this.horizon) / (2 * this.horizonRange);
    }

    private double mapAlpha(double alpha) {
        if (alpha > 180) {
            return alpha - 360;
        } else {
            return alpha;
        }
    }

    private double gamma(double alpha, double x, double y, double x_object, double y_object) {
        alpha = mapAlpha(alpha);

        double h1 = getDistance(x_object, y_object, x, y_object);
        double h2 = getDistance(x_object, y_object, x, y);

        double beta;
        if (h2 != 0) {
            beta = Math.asin(h1 / h2) * 180.0 / Math.PI;
        } else {
            beta = 0;
        }

        if (y_object < y) {
            beta += 90;
        }
        if (x_object < x) {
            beta *= -1;
        }

        return beta - alpha;
    }
}
